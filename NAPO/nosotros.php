<!DOCTYPE HTML>
<html>
	<head>
            <title>TANDA + | ¿Estas cansado de lo mismo?</title>
            <meta charset="utf-8" />
            <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="stylesheet" href="css/main.css" />
	</head>
	<body class="homepage">
		<div id="page-wrapper">

            <!-- Header -->            
            <div id="header-wrapper">
                <div id="header" class="container">

                    <!-- Logo -->
                    <h1 id="logo"><a href="index.php">tanda <i class="icon fa-plus" style="font-size:70%;"></i></a></h1>

                    <!-- Nav -->
                    <nav id="nav">
                        <ul>
                            <li><a href="ayuda.php#seccionAyuda">Ayuda</a></li>
                            <li>
                                <a href="nosotros.php#seccionNosotros">Nosotros</a>
                            </li>
                            <li class="break"><a href="ingresar.php#seccionAcceso">Ingresar</a></li>
                            <li><a href="registro.php#seccionRegistro">Registro</a></li>
                        </ul>
                    </nav>

                </div>

                <!-- Hero -->
                <section id="hero" class="container">
                    <header>
                        <img src="images/logo.png">
                        <h2>Bienvenido a <strong>tanda <i class="icon fa-plus" style="font-size:70%;"></i> </strong>
                            <br />
                            sitio de tandas online lider en M&eacute;xico.</h2>
                    </header>
                    <p>Si buscar mejorar tus <strong>ingresos</strong> y tu <strong>calidad de vida</strong>
                        <br />
                        <strong>has llegado al lugar indicado </strong></p>
                    <ul class="actions">
                        <li>&copy; <strong>tanda <i class="icon fa-plus" style="font-size:50%;"></i></strong> S.A. de R.L.  Todos los derechos reservados.</li>
                    </ul>
                </section>

            </div>

    <!-- Footer -->
    <a name="seccionNosotros"></a> 
        <div id="footer-wrapper" style="padding-top:50px;">
            <div id="footer" class="container" >
                        <header class="major">
                            <h2>Ya conoces las tandas, <br>es por eso que sabemos lo que necesitas</h2>
                            <h4>En tanda+ obtienes 16 veces lo que ahorras</h4>
                                
                        </header>
                        <div class="row">
                                <section class="6u 12u(mobile)" >
                                    <div align="center">
                                        <ul align="center">
                                            <img src="images/juanEscritorio.png" style="width:70%;"> 
                                        </ul>
                                    </div>
                                </section>

                                <section class="6u 12u(narrower)">
                                    
                                    <h6>
                                     
                                    mas ahorro esto va gradualmente mas grande, 
                                    <br />
                                    Mas beneficios
                                    <br />
                                    Mas oportunidades
                                    <br />
                                    MAS Rico
                                    <br />
                                    MAS Rapido
                                    <br />
                                    MAS GANACIAS.
                                    <br />
                                    MAS DINERO. esto quedaria grandotote (cambiar todos los mas por +)
                                    <br />
                                    ¿Quieres saber más y hacer parte de este mundo tan maravilloso?
                                    <br>
                                    ¡UNETE!

                                        
                                    </h6>
                                </section>
                        </div>
                </div>  
                <div id="copyright" class="container">
                <ul class="menu">
                   <li>&copy; <strong>tanda <i class="icon fa-plus" style="font-size:50%;"></i></strong> S.A. de R.L.  Todos los derechos reservados.</li>
                </ul>
            </div>
				</div>

		</div>

		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>