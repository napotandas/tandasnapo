<!DOCTYPE HTML>
<?php 
error_reporting(E_ALL ^ E_NOTICE);
include ("./backend/sesiones/sesion.php"); 
?>
<html>
	<head>
		<title>NAPO | NOTIFICACIONES</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="css/main2.css" />
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body id="top">

		<!-- Header -->
			<header id="header">
				<a href="usuarioInicio.php" class="image avatar"><img src="images/avatar.jpg" alt="" /></a>
                                <h1><strong>Usuario: </strong><?php echo ($_SESSION["aliasUsuario"]);?><br />
				Nivel: <?php echo ($_SESSION["nivelUsuario"]);?><br /><br/>
				Etapa: <?php echo ($_SESSION["etapaUsuario"]);?><br />
                                </h1>
			</header>
               <!-- Main User -->
               <div id="main-user">    
                   <ul class="nav">
                       <li><a href="#"><i class="fa fa-user"></i> Mi Cuenta</a>
                            <ul>
                                <li><a href="usuarioConfig.php">Ajustes</a></li>
                                <li><a href="index.php">Salir</a></li>
                            </ul>                      
                        </li>
                       
                        <li><a href="#"><i class="fa fa-bell"></i> Notiificaciones</a>
                            <ul>
                                <li><a href="usuarioNotificaciones.php">...</a></li>
                            </ul>                      
                        </li>        
                       
                        <li><a href="">Ayuda</a>
                            <ul>
                                <li><a href="usuarioSoporte.php">Soporte</a></li>
                                <li><a href="usuarioPF.php">Preguntas Frecuentes</a></li>
                            </ul>
                        </li>   
                   </ul>
                </div>
               
                <!-- Main -->
			<div id="main">
                            
					<section id="two">
						<h2>Tus usuarios pendientes de activar son:</h2>
						<div class="row">
                                                    <ul>
							<li class="usrAct">
                                                                <a href="#" class="image avatar"><img src="images/avatar.jpg" alt="" /></a>
                                                                <h3>B3b3c!t00p 3m0X1t0000p..!</h3>
                                                                <p>bebeciitoemoxitoooop@bebecitosemoxitos.com<br><small>7555689425</small></p>
                                                                
                                                                
                                                                <a href="#" class="button icon fa-thumbs-o-up" >Activar</a>
							</li>
                                                    
							<li class="usrAct">
                                                                <a href="#" class="image avatar"><img src="images/avatar.jpg" alt="" /></a>
								<h3>La Reata 69</h3>
								<p>ElverGalarga@losvergas.com<br><small>7555689425</small></p>
                                                                <a href="#" class="button icon fa-thumbs-o-up" >Activar</a>
							</li>
                                                    
							<li class="usrAct">
                                                                <a href="#" class="image avatar"><img src="images/avatar.jpg" alt="" /></a>
								<h3>Tashas y Perico</h3>
								<p>ExtasisyCocaina@drogas.com<br><small>7555689425</small></p>
                                                                <a href="#" class="button icon fa-thumbs-o-up" >Activar</a>
							</li>
                                                    
							<li class="usrAct">
                                                                <a href="#" class="image avatar"><img src="images/avatar.jpg" alt="" /></a>
								<h3>Vamos a Pistear</h3>
								<p>yYoEstoyAquiBorrachoYLoco@borrachos.com<br><small>7555689425</small></p>
                                                                <a href="#" class="button icon fa-thumbs-o-up" >Activar</a>
							</li>
                                                    </ul>
						</div>
						
					</section>
				<!-- Three -->
				
                    <section id="three">
                        <h2>Tus Datos</h2>
                            <div class="">
                                <ul class="labeled-icons">
                                    <li>
                                            <h3 class="icon fa-male"><span class="label">Usuario:</span></h3>
                                            <?php echo ($_SESSION["aliasUsuario"]);?>
                                    </li>
                                    <li>
                                        <h3 class="icon fa-mobile"><span class="label">Tu tel&eacute:fono</span></h3>
                                            <?php echo ($_SESSION["telefonoUsuario"]);?>
                                    </li>
                                    <li>
                                            <h3 class="icon fa-envelope-o"><span class="label">Tu Email:</span></h3>
                                            <a href="#"><?php echo ($_SESSION["emailUsuario"]);?></a>
                                    </li>
                                </ul>
                            </div>
                    </section>
                                <!-- Four -->
<!--
				 
				
					<section id="four">
						<h2>Elements</h2>



						<section>
							<h4>Table</h4>
							<h5>Default</h5>
							<div class="table-wrapper">
								<table>
									<thead>
										<tr>
											<th>Name</th>
											<th>Description</th>
											<th>Price</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Item One</td>
											<td>Ante turpis integer aliquet porttitor.</td>
											<td>29.99</td>
										</tr>
										<tr>
											<td>Item Two</td>
											<td>Vis ac commodo adipiscing arcu aliquet.</td>
											<td>19.99</td>
										</tr>
										<tr>
											<td>Item Three</td>
											<td> Morbi faucibus arcu accumsan lorem.</td>
											<td>29.99</td>
										</tr>
										<tr>
											<td>Item Four</td>
											<td>Vitae integer tempus condimentum.</td>
											<td>19.99</td>
										</tr>
										<tr>
											<td>Item Five</td>
											<td>Ante turpis integer aliquet porttitor.</td>
											<td>29.99</td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="2"></td>
											<td>100.00</td>
										</tr>
									</tfoot>
								</table>
							</div>

							<h5>Alternate</h5>
							<div class="table-wrapper">
								<table class="alt">
									<thead>
										<tr>
											<th>Name</th>
											<th>Description</th>
											<th>Price</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Item One</td>
											<td>Ante turpis integer aliquet porttitor.</td>
											<td>29.99</td>
										</tr>
										<tr>
											<td>Item Two</td>
											<td>Vis ac commodo adipiscing arcu aliquet.</td>
											<td>19.99</td>
										</tr>
										<tr>
											<td>Item Three</td>
											<td> Morbi faucibus arcu accumsan lorem.</td>
											<td>29.99</td>
										</tr>
										<tr>
											<td>Item Four</td>
											<td>Vitae integer tempus condimentum.</td>
											<td>19.99</td>
										</tr>
										<tr>
											<td>Item Five</td>
											<td>Ante turpis integer aliquet porttitor.</td>
											<td>29.99</td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="2"></td>
											<td>100.00</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</section>

						<section>
							<h4>Buttons</h4>
							<ul class="actions">
								<li><a href="#" class="button special">Special</a></li>
								<li><a href="#" class="button">Default</a></li>
							</ul>
							<ul class="actions">
								<li><a href="#" class="button big">Big</a></li>
								<li><a href="#" class="button">Default</a></li>
								<li><a href="#" class="button small">Small</a></li>
							</ul>
							<ul class="actions fit">
								<li><a href="#" class="button special fit">Fit</a></li>
								<li><a href="#" class="button fit">Fit</a></li>
							</ul>
							<ul class="actions fit small">
								<li><a href="#" class="button special fit small">Fit + Small</a></li>
								<li><a href="#" class="button fit small">Fit + Small</a></li>
							</ul>
							<ul class="actions">
								<li><a href="#" class="button special icon fa-download">Icon</a></li>
								<li><a href="#" class="button icon fa-download">Icon</a></li>
							</ul>
							<ul class="actions">
								<li><span class="button special disabled">Special</span></li>
								<li><span class="button disabled">Default</span></li>
							</ul>
						</section>

						<section>
							<h4>Form</h4>
							<form method="post" action="#">
								<div class="row uniform 50%">
									<div class="6u 12u$(xsmall)">
										<input type="text" name="demo-name" id="demo-name" value="" placeholder="Name" />
									</div>
									<div class="6u$ 12u$(xsmall)">
										<input type="email" name="demo-email" id="demo-email" value="" placeholder="Email" />
									</div>
									<div class="12u$">
										<div class="select-wrapper">
											<select name="demo-category" id="demo-category">
												<option value="">- Category -</option>
												<option value="1">Manufacturing</option>
												<option value="1">Shipping</option>
												<option value="1">Administration</option>
												<option value="1">Human Resources</option>
											</select>
										</div>
									</div>
									<div class="4u 12u$(small)">
										<input type="radio" id="demo-priority-low" name="demo-priority" checked>
										<label for="demo-priority-low">Low Priority</label>
									</div>
									<div class="4u 12u$(small)">
										<input type="radio" id="demo-priority-normal" name="demo-priority">
										<label for="demo-priority-normal">Normal Priority</label>
									</div>
									<div class="4u$ 12u(small)">
										<input type="radio" id="demo-priority-high" name="demo-priority">
										<label for="demo-priority-high">High Priority</label>
									</div>
									<div class="6u 12u$(small)">
										<input type="checkbox" id="demo-copy" name="demo-copy">
										<label for="demo-copy">Email me a copy of this message</label>
									</div>
									<div class="6u$ 12u$(small)">
										<input type="checkbox" id="demo-human" name="demo-human" checked>
										<label for="demo-human">I am a human and not a robot</label>
									</div>
									<div class="12u$">
										<textarea name="demo-message" id="demo-message" placeholder="Enter your message" rows="6"></textarea>
									</div>
									<div class="12u$">
										<ul class="actions">
											<li><input type="submit" value="Send Message" class="special" /></li>
											<li><input type="reset" value="Reset" /></li>
										</ul>
									</div>
								</div>
							</form>
						</section>

					</section>
-->				

			</div>

		<!-- Footer -->
			<footer id="footer">
				
				<ul class="copyright">
					<li>&copy; NAPO S.A de C.V</li>
                                        <li><a href="mailito:soporte@napo.com.mx">soporte@napo.com.mx</a></li>
                                        
				</ul>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>