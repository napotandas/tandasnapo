<!DOCTYPE HTML>
<html>
	<head>
            <title>TANDA + | Validar Cuenta</title>
        <meta charset="utf-8" />
        <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/main.css" />
	</head>
	<body class="homepage">
		<div id="page-wrapper">

            <!-- Header -->            
            <div id="header-wrapper" style="padding: 40px;">
                    <!-- Logo -->
                    <h1 id="logo"><a href="index.php">tanda <i class="icon fa-plus" style="font-size:70%;"></i></a></h1>

            </div>

<!-- Footer -->
<a name="seccionRecoperar"></a> 
    <div   id="footer-wrapper" style="padding-top:30px;">
        <div align="center" id="footer" class="container">
            <header class="">
                <h2>Tu cuenta de <strong>tanda <i class="icon fa-plus" style="font-size:50%;"></i></strong> casi está lista...</h2>
                <p style="color: #27636B;">¡Solo te falta validarla!</p>
                <h3>Te hemos enviado un enlace al correo que ingresaste. <br> <strong>¡Solo &aacute;brelo y listo!</strong><br></h3>
            </header>  
                        
                <div align="center" style="margin: -20px; padding-top: 5%;">
                    <ul>
                        <img src="images/logo3.png"> 
                    </ul>
                </div>
            
                    
            
        </div>  
            <div id="copyright" class="container">
            <ul class="menu">
               <li>&copy; <strong>tanda <i class="icon fa-plus" style="font-size:50%;"></i></strong> S.A. de R.L.  Todos los derechos reservados.</li>
            </ul>
        </div>
    </div>

		</div>

		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>