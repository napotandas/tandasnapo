<!DOCTYPE HTML>

<html>
    <head>
        <title>TANDA +</title>
        <meta charset="utf-8" />
        <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/main.css" />
    </head>
    <body class="homepage">
        <div id="page-wrapper">

            <!-- Header -->
            <div id="header-wrapper">
                <div id="header" class="container">

                    <!-- Logo -->
                    <h1 id="logo"><a href="index.php">tanda <i class="icon fa-plus" style="font-size:70%;"></i></a></h1>

                    <!-- Nav -->
                    <nav id="nav">
                        <ul>
                            <li><a href="nosotros.php#seccionNosotros">¿Que es?</a></li>
                            <li>
                                <a href="ayuda.php#seccionAyuda">¿Como Funciona?</a>
                            </li>
                            <li class="break"><a href="ingresar.php#seccionAcceso">Ingresar</a></li>
                            <li><a href="registro.php#seccionRegistro">Registro</a></li>
                        </ul>
                    </nav>

                </div>

                <!-- Hero -->
                <section id="hero" class="container">
                    <header>
                        <img src="images/logo.png">
                        <h2>Bienvenido a <strong>tanda <i class="icon fa-plus" style="font-size:70%;"></i> </strong>
                            <br />
                            sitio de tandas online lider en M&eacute;xico.</h2>
                    </header>
                    <p>Si buscar mejorar tus <strong>ingresos</strong> y tu <strong>calidad de vida</strong>
                        <br />
                        <strong>has llegado al lugar indicado </strong></p>
                    <ul class="actions">
                        <li><a href="ayuda.php#seccionAyuda" class="button">¡M&uacute;estrame c&oacute;mo!</a></li>
                    </ul>
                </section>

            </div>

            <!-- Features 2 -->
            <div class="wrapper">
                <section class="container">
                    <header class="major">
                        <h2>Conoce algunas historias de &eacute;xito de personas como t&uacute;</h2>
                      
                    </header>
                    <div class="row features">
                        <section class="4u 12u(narrower) feature">
                            <div class="image-wrapper first">
                                <a href="#" class="image featured"><img src="images/pic05.jpg" alt="" /></a>
                            </div>
                            <p>Con <strong>tanda <i class="icon fa-plus" style="font-size:50%;"></i> </strong>, mis ingresos semanales aumentaron, y ahora puedo incluso
                                <strong>darme un gusto de vez en cuando</strong></p>
                        </section>
                        <section class="4u 12u(narrower) feature">
                            <div class="image-wrapper">
                                <a href="#" class="image featured"><img src="images/pic03.jpg" alt="" /></a>
                            </div>
                            <p>Conoc&iacute; al diseñador de <strong>tanda <i class="icon fa-plus" style="font-size:50%;"></i> </strong>, esta bien pinche sabroso, terminamos en su casa y
                            me dio de perrito sabrosamente</p>
                        </section>
                        <section class="4u 12u(narrower) feature">
                            <div class="image-wrapper">
                                <a href="#" class="image featured"><img src="images/pic09.jpg" alt="" /></a>
                            </div>
                            <p>Al llegar al nivel 6, en menos de una semana junt&eacute;a mis 8 personas
                                y recib&iacute; 24 mil varitos, me alcanzo para 400 gramos de Caspita del Diablo,
                                que me <strong>meti por la nariz</strong></p>
                        </section>
                    </div>
                    <ul class="actions major">
                        <li><a href="#" class="button">Volver arriba</a></li>
                    </ul>
                </section>
            </div>

            <!-- Promo -->
            <div id="promo-wrapper">
                <section id="promo">
                    <h2> ¿A&uacute;n tienes dudas? Visita la p&aacute;gina de ayuda</h2>
                    <a href="ayuda.php#seccionAyuda" class="button">Ir all&aacute;</a>
                </section>
            </div>


            <div id="copyright" class="container">
                <ul class="menu">
                    <li>&copy; <strong>tanda <i class="icon fa-plus" style="font-size:50%;"></i></strong> S.A. de R.L.  Todos los derechos reservados.</li>
                </ul>
            </div>
        </div>

        <!-- Scripts -->

        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
        <script src="assets/js/main.js"></script>

    </body>
</html>