<!DOCTYPE HTML>
<?php 
error_reporting(E_ALL ^ E_NOTICE);
include ("./backend/sesiones/sesion.php"); 
?>
<html>
	<head>
		<title>NAPO | Configuración</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="css/main2.css" />
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body id="top">

		<!-- Header -->
			<header id="header">
				<a href="usuarioInicio.php" class="image avatar"><img src="images/avatar.jpg" alt="" /></a>
                                <h1><strong>Usuario: </strong><?php echo ($_SESSION["aliasUsuario"]);?><br />
				Nivel: <?php echo ($_SESSION["nivelUsuario"]);?><br /><br/>
				Etapa: <?php echo ($_SESSION["etapaUsuario"]);?><br />
                                </h1>
			</header>
               <!-- Main User -->
               <div id="main-user">    
                   <ul class="nav">
                       <li><a href="#"><i class="fa fa-user"></i> Mi Cuenta</a>
                            <ul>
                                <li><a href="usuarioConfig.php">Ajustes</a></li>
                                <li><a href="index.php">Salir</a></li>
                            </ul>                      
                        </li>
                       
                        <li><a href="#"><i class="fa fa-bell"></i> Notiificaciones</a>
                            <ul>
                                <li><a href="usuarioNotificaciones.php">...</a></li>
                            </ul>                      
                        </li>        
                       
                        <li><a href="">Ayuda</a>
                            <ul>
                                <li><a href="usuarioSoporte.php">Soporte</a></li>
                                <li><a href="usuarioPF.php">Preguntas Frecuentes</a></li>
                            </ul>
                        </li>   
                   </ul>
                </div>
               
                <!-- Main -->
                <div id="main">
		
					<section id="three">
                                            <h2>Eliminar Cuenta</h2>

                                            <h5>
                                                Porfavor Selecciona un motivo
                                                
                                            </h5>
                                            <form method="post" action="#">
                                                <div >
                                                    <fieldset>
                                                    <div class="6u 12u(small)">
                                                        <input type="radio" id="MotivoUno" name="motivo" >
                                                         <label for="MotivoUno">Tengo otra cuenta en NAPO.</label>
                                                     </div>
                                                    
                                                    <div class="8u 12u(small)">
                                                         <input type="radio" id="MotivoDos" name="motivo"  >
                                                         <label for="MotivoDos">Me siento inseguro o tengo un problema con mi cuenta en NAPO.</label>
                                                     </div>
                                                    
                                                    <div class="8u 12u(small)">
                                                         <input type="radio" id="MotivoTres" name="motivo">
                                                         <label for="MotivoTres">Me preocupa mi seguridad en NAPO.</label>
                                                     </div>
                                                    
                                                    <div class="8u 12u(small)">
                                                         <input type="radio" id="MotivoCuatro" name="motivo" >
                                                         <label for="MotivoCuatro">Tengo la intencion de crear otra cuenta en el futuro.</label>
                                                     </div>
                                                    
                                                    <div class="8u 12u(small)">
                                                         <input type="radio" id="MotivoCinco" name="motivo" >
                                                         <label for="MotivoCinco">Mi cuenta no me resulta util.</label>
                                                     </div>
                                                    
                                                    <div class="8u 12u(small)">
                                                         <input type="radio" id="MotivoSeis" name="motivo" >
                                                         <label for="MotivoSeis">No se como utilizar NAPO.</label>
                                                     </div>
                                                    
                                                    <div class="8u 12u(small)">
                                                         <input type="radio" id="MotivoSiete" name="motivo" >
                                                         <label for="MotivoSiete">Otro.</label>
                                                     </div>
                                                    
                                                     <div class="12u">
                                                             <textarea name="OtroMotivo" id="demo-message" placeholder="Escribe tu motivo" rows="6"></textarea>
                                                     </div>
                                                    <br>
                                                    
                                                    <h4>Introducir Contraseña</h4>
                              
                                                    <div class="row uniform 50%">
                                                        <div class="6u 12u(xsmall)">
                                                            <input type="password" name="contraseña" placeholder="Contraseña" />
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="12u" >
                                                             <ul class="actions">
                                                                 <li><input type="submit" value="Enviar" class="special"/></li>
                                                             </ul>
                                                     </div>
                                                    </fieldset>
                                                </div>
                                            </form>
					</section>
                    

              
	</div>

		<!-- Footer -->
			<footer id="footer">
				
				<ul class="copyright">
					<li>&copy; NAPO S.A de C.V</li>
                                        <li><a href="mailito:soporte@napo.com.mx">soporte@napo.com.mx</a></li>
                                        
				</ul>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>