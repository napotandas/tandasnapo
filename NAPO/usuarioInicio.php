<!DOCTYPE HTML>
<?php
error_reporting(E_ALL ^ E_NOTICE);
include ("./backend/sesiones/sesion.php");
include ("/backend/conexion.php");
$con = new conexion();
$con->abrir();
?>
<html>
    <head>
        <title>TANDA + | MI CUENTA</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
        <link rel="stylesheet" href="css/main2.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/joint.css" />
        <link rel="stylesheet" type="text/css" href="css/joint.min.css.css" />
        <script src="js/jquery.min.js"></script>
        <script src="js/lodash.min.js"></script>
        <script src="js/backbone-min.js"></script>
        <script src="js/joint.js"></script>
        <script src="js/joint.min.js.js"></script>
    </head>
    <body id="top">
        <!-- Header -->
        <header id="header">
            <a href="usuarioInicio.php" class="image avatar"><img src="images/avatar.jpg" alt="" /></a>
            <h1><strong>Usuario: </strong><?php echo ($_SESSION["aliasUsuario"]); ?><br />
                Nivel: <?php
                print ($_SESSION["emailUsuario"]);
                ?><br /><br/>
                Etapa: <?php echo ($_SESSION["telefonoUsuario"]); ?><br />
            </h1>

            <h4>Invita a tus conocidos a tu arbol</h4>
            <form method="post" action="/tandasnapo/NAPO//backend/usuarios/invitarCorreo.php">

                <input type="email" name="email-invitar" placeholder="E-mail de tu referido" class="special" />
                <div class="12u">
                    <ul class="actions">
                        <li><input type="submit" value="Invitar" class="special" /></li>
                        <li><input type="reset" value="Limpiar Campo" /></li>
                    </ul>
                </div>
            </form>
        </header>
        <!-- Main User -->
        <div id="main-user">    
            <ul class="nav">
                <li><a href="#"><i class="fa fa-user"></i> Mi Cuenta</a>
                    <ul>
                        <li><a href="usuarioConfig.php">Ajustes</a></li>
                        <li><a href="/tandasnapo/NAPO/backend/sesiones/salir.php">Salir</a></li>
                    </ul>                      
                </li>
                <li><a href="#"><i class="fa fa-bell"></i> Notiificaciones</a>
                    <ul>
                        <li><a href="usuarioNotificaciones.php">...</a></li>
                    </ul>                      
                </li>      
                <li><a href="">Ayuda</a>
                    <ul>
                        <li><a href="usuarioSoporte.php">Soporte</a></li>
                        <li><a href="usuarioPF.php">Preguntas Frecuentes</a></li>
                    </ul>
                </li>   
            </ul>
        </div>

        <!-- Main -->
        <div id="main">
            <section>
                <h2>Mis Tablas</h2>
                <div class="box alt">
                    <div class="row 50% uniform">
                        <div id="Arbol" style="display:inline; pointer-events: none "></div>
                        <!--<div class="12u"><span class="image fit"><img src="images/arbol.png" alt="" /></span></div>-->
                    </div>
                </div>
                <h5 id="ficha">ID:  568947</h5>
            </section>
            <section id="">
                <h2>Tus Datos</h2>
                <div class="">
                    <ul class="labeled-icons">
                        <li>
                            <h3 class="icon fa-male"><span class="label">Usuario:</span></h3>
                            <?php echo ($_SESSION["aliasUsuario"]); ?>
                        </li>
                        <li>
                            <h3 class="icon fa-mobile"><span class="label">Tu tel&eacute:fono</span></h3>
                            <?php echo ($_SESSION["telefonoUsuario"]); ?>
                        </li>
                        <li>
                            <h3 class="icon fa-envelope-o"><span class="label">Tu Email:</span></h3>
                            <a href="#"><?php echo ($_SESSION["emailUsuario"]); ?></a>
                        </li>
                    </ul>
                </div>
               
                <!-- Main -->
			<div id="main">
                            <section>
                                    <h2>Mis Tablas</h2>
                                    <div class="box alt">
                                        <div class="row 50% uniform">
                                            <div id="Arbol" style="display:inline; pointer-events: none "></div>
                                            <!--<div class="12u"><span class="image fit"><img src="images/arbol.png" alt="" /></span></div>-->
                                        </div>
                                    </div>
                                    <h5 id="ficha">ID:  568947</h5>
                                    <h4>Invita a tus conocidos a tu arbol</h4>
                                    <form method="post" action="/tandasnapo/NAPO//backend/usuarios/invitarCorreo.php">

                                        <input type="email" name="email-invitar" placeholder="E-mail de tu referido" class="special" />
                                        <div class="12u" >
                                            <ul class="actions">
                                                <li><input type="submit" value="Invitar" class="special" /></li>
                                                <li><input type="reset" value="Limpiar Campo" /></li>
                                            </ul>
                                        </div>
                                    </form>
                            </section>
                            <section id="">
                                    <h2>Tus Datos</h2>
                                         <div class="">
                                            <ul class="labeled-icons">
                                                <li>
                                                        <h3 class="icon fa-male"><span class="label">Usuario:</span></h3>
                                                        <?php echo ($_SESSION["aliasUsuario"]);?>
                                                </li>
                                                <li>
                                                    <h3 class="icon fa-mobile"><span class="label">Tu tel&eacute:fono</span></h3>
                                                        <?php echo ($_SESSION["telefonoUsuario"]);?>
                                                </li>
                                                <li>
                                                        <h3 class="icon fa-envelope-o"><span class="label">Tu Email:</span></h3>
                                                        <a href="#"><?php echo ($_SESSION["emailUsuario"]);?></a>
                                                </li>
                                            </ul>
                                        </div>
                            </section>
			</div>

		<!-- Footer -->
			<footer id="footer">
				
				<ul class="copyright">
                                    <li>&copy; tanda <i class="icon fa-plus" style="font-size:60%;"></i></li>
                                    <li><a href="mailito:soporte@napo.com.mx">soporte@napo.com.mx</a></li>
				</ul>
			</footer>
 
		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

                <!-- ArbolPrincipal -->        
                        <script>
                        
                                var graph = new joint.dia.Graph();

                                var paper = new joint.dia.Paper({
                                    el: $('#Arbol'),
                                    width: 800,
                                    height: 450,
                                    gridSize: 1,
                                    model: graph,
                                    perpendicularLinks: true,
                                    restrictTranslate: true
                                });

                                var member = function(x, y, rank, name, image, background, textColor) {

                                    textColor = textColor || "#000";

                                    var cell = new joint.shapes.org.Member({
                                        position: { x: x, y: y },
                                        attrs: {
                                            '.card': { fill: background, stroke: 'none'},
                                              image: { 'xlink:href': 'images/'+ image, opacity: 0.7 },
                                            '.rank': { text: rank, fill: textColor, 'word-spacing': '-5px', 'letter-spacing': 0},
                                            '.name': { text: name, fill: textColor, 'font-size': 13, 'font-family': 'Arial', 'letter-spacing': 0 }
                                        }
                                    });
                                    graph.addCell(cell);
                                    return cell;
                                };

                                function link(source, target, breakpoints) {

                                    var cell = new joint.shapes.org.Arrow({
                                        source: { id: source.id },
                                        target: { id: target.id },
                                        vertices: breakpoints,
                                        attrs: {
                                            '.connection': {
                                                'fill': 'none',
                                                'stroke-linejoin': 'round',
                                                'stroke-width': '2',
                                                'stroke': '#4b4a67'
                                            }
                                        }

                                    });
                                    graph.addCell(cell);
                                    return cell;
                                }

                                var ATel =      "<?php echo $_SESSION['ATel']; ?>";
                                var AUser =     "<?php echo $_SESSION['AUser']; ?>";
                                var BTel =      "<?php echo $_SESSION['BTel']; ?>";
                                var BUser =     "<?php echo $_SESSION['BUser']; ?>";
                                var CTel =      "<?php echo $_SESSION['CTel']; ?>";
                                var CUser =     "<?php echo $_SESSION['CUser']; ?>";
                                var B1Tel =     "<?php echo $_SESSION['B1Tel']; ?>";
                                var B1User =    "<?php echo $_SESSION['B1User']; ?>";
                                var B2Tel =     "<?php echo $_SESSION['B2Tel']; ?>";
                                var B2User =    "<?php echo $_SESSION['B2User']; ?>";
                                var C1Tel =     "<?php echo $_SESSION['C1Tel']; ?>";
                                var C1User =    "<?php echo $_SESSION['C1User']; ?>";
                                var C2Tel =     "<?php echo $_SESSION['C2Tel']; ?>";
                                var C2User =    "<?php echo $_SESSION['C2User']; ?>";
                                
                                
=======
            </section>
        </div>

        <!-- Footer -->
        <footer id="footer">

            <ul class="copyright">
                <li>&copy; tanda <i class="icon fa-plus" style="font-size:60%;"></i></li>
                <li><a href="mailito:soporte@napo.com.mx">soporte@napo.com.mx</a></li>
            </ul>
        </footer>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.poptrox.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
        <script src="assets/js/main.js"></script>

        <!-- ArbolPrincipal -->        
        <script>

            var graph = new joint.dia.Graph();

            var paper = new joint.dia.Paper({
                el: $('#Arbol'),
                width: 800,
                height: 450,
                gridSize: 1,
                model: graph,
                perpendicularLinks: true,
                restrictTranslate: true
            });

            var member = function (x, y, rank, name, image, background, textColor) {

                textColor = textColor || "#000";

                var cell = new joint.shapes.org.Member({
                    position: {x: x, y: y},
                    attrs: {
                        '.card': {fill: background, stroke: 'none'},
                        image: {'xlink:href': 'images/' + image, opacity: 0.7},
                        '.rank': {text: rank, fill: textColor, 'word-spacing': '-5px', 'letter-spacing': 0},
                        '.name': {text: name, fill: textColor, 'font-size': 13, 'font-family': 'Arial', 'letter-spacing': 0}
                    }
                });
                graph.addCell(cell);
                return cell;
            };
>>>>>>> 8826941156bb3d4a2a8eed86d7a3b1400ab0ed9c

            function link(source, target, breakpoints) {

                var cell = new joint.shapes.org.Arrow({
                    source: {id: source.id},
                    target: {id: target.id},
                    vertices: breakpoints,
                    attrs: {
                        '.connection': {
                            'fill': 'none',
                            'stroke-linejoin': 'round',
                            'stroke-width': '2',
                            'stroke': '#4b4a67'
                        }
                    }

                });
                graph.addCell(cell);
                return cell;
            }

            var ATel = "<?php echo $_SESSION['ATel']; ?>";
            var AUser = "<?php echo $_SESSION['AUser']; ?>";
            var BTel = "<?php echo $_SESSION['BTel']; ?>";
            var BUser = "<?php echo $_SESSION['BUser']; ?>";
            var CTel = "<?php echo $_SESSION['CTel']; ?>";
            var CUser = "<?php echo $_SESSION['CUser']; ?>";
            var B1Tel = "<?php echo $_SESSION['B1Tel']; ?>";
            var B1User = "<?php echo $_SESSION['B1User']; ?>";
            var B2Tel = "<?php echo $_SESSION['B2Tel']; ?>";
            var B2User = "<?php echo $_SESSION['B2User']; ?>";
            var C1Tel = "<?php echo $_SESSION['C1Tel']; ?>";
            var C1User = "<?php echo $_SESSION['C1User']; ?>";
            var C2Tel = "<?php echo $_SESSION['C2Tel']; ?>";
            var C2User = "<?php echo $_SESSION['C2User']; ?>";



            var A = member(300, 70, ATel, AUser, '1.png', '#910281');
            var B = member(90, 200, BTel, BUser, '2.png', '#BAD80A');
            var C = member(500, 200, CTel, CUser, '3.png', '#107B11');
            var B1 = member(0, 350, B1Tel, B1User, '4.png', '#FFCC00');
            var B2 = member(190, 350, B2Tel, B2User, '5.png', '#FFCC00');
            var C1 = member(400, 350, C1Tel, C1User, '6.png', '#00CCCC');
            var C2 = member(590, 350, C2Tel, C2User, '7.png', '#00CCCC');



            link(A, C, [{x: 385, y: 180}, {x: 600, y: 180}]);
            link(A, B, [{x: 385, y: 180}, {x: 175, y: 180}]);
            link(B, B1, [{x: 175, y: 330}, {x: 85, y: 330}]);
            link(B, B2, [{x: 175, y: 330}, {x: 270, y: 330}]);
            link(C, C1, [{x: 600, y: 330}, {x: 485, y: 330}]);
            link(C, C2, [{x: 600, y: 330}, {x: 685, y: 330}]);
        </script>

    </body>
</html>






<!-- Two --
                                        <section id="two">
                                                <h2>Tus usuarios pendientes de activar son:</h2>
                                                <div class="row">
                                                        <article class="">
                                                                <a href="images/fulls/01.jpg" class="image fit thumb"><img src="images/thumbs/01.jpg" alt="" /></a>
                                                                <h3>B3b3c!t00p 3m0X1t0000p..!</h3>
                                                                <p>bebeciitoemoxitoooop@bebecitosemoxitos.com</p>
                                                        </article>
                                                        <article class="">
                                                                <a href="images/fulls/02.jpg" class="image fit thumb"><img src="images/thumbs/02.jpg" alt="" /></a>
                                                                <h3>La Reata 69</h3>
                                                                <p>ElverGalarga@losvergas.com</p>
                                                        </article>
                                                        <article class="">
                                                                <a href="images/fulls/03.jpg" class="image fit thumb"><img src="images/thumbs/03.jpg" alt="" /></a>
                                                                <h3>Tashas y Perico</h3>
                                                                <p>ExtasisyCocaina@drogas.com</p>
                                                        </article>
                                                        <article class="">
                                                                <a href="images/fulls/04.jpg" class="image fit thumb"><img src="images/thumbs/04.jpg" alt="" /></a>
                                                                <h3>Vamos a Pistear</h3>
                                                                <p>yYoEstoyAquiBorrachoYLoco@borrachos.com</p>
                                                        </article>
                                                        
                                                </div>
                                                
                                        </section>
-->
<!-- Four -->
<!--
                                 
                                
                                        <section id="four">
                                                <h2>Elements</h2>



                                                <section>
                                                        <h4>Table</h4>
                                                        <h5>Default</h5>
                                                        <div class="table-wrapper">
                                                                <table>
                                                                        <thead>
                                                                                <tr>
                                                                                        <th>Name</th>
                                                                                        <th>Description</th>
                                                                                        <th>Price</th>
                                                                                </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                                <tr>
                                                                                        <td>Item One</td>
                                                                                        <td>Ante turpis integer aliquet porttitor.</td>
                                                                                        <td>29.99</td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td>Item Two</td>
                                                                                        <td>Vis ac commodo adipiscing arcu aliquet.</td>
                                                                                        <td>19.99</td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td>Item Three</td>
                                                                                        <td> Morbi faucibus arcu accumsan lorem.</td>
                                                                                        <td>29.99</td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td>Item Four</td>
                                                                                        <td>Vitae integer tempus condimentum.</td>
                                                                                        <td>19.99</td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td>Item Five</td>
                                                                                        <td>Ante turpis integer aliquet porttitor.</td>
                                                                                        <td>29.99</td>
                                                                                </tr>
                                                                        </tbody>
                                                                        <tfoot>
                                                                                <tr>
                                                                                        <td colspan="2"></td>
                                                                                        <td>100.00</td>
                                                                                </tr>
                                                                        </tfoot>
                                                                </table>
                                                        </div>

                                                        <h5>Alternate</h5>
                                                        <div class="table-wrapper">
                                                                <table class="alt">
                                                                        <thead>
                                                                                <tr>
                                                                                        <th>Name</th>
                                                                                        <th>Description</th>
                                                                                        <th>Price</th>
                                                                                </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                                <tr>
                                                                                        <td>Item One</td>
                                                                                        <td>Ante turpis integer aliquet porttitor.</td>
                                                                                        <td>29.99</td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td>Item Two</td>
                                                                                        <td>Vis ac commodo adipiscing arcu aliquet.</td>
                                                                                        <td>19.99</td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td>Item Three</td>
                                                                                        <td> Morbi faucibus arcu accumsan lorem.</td>
                                                                                        <td>29.99</td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td>Item Four</td>
                                                                                        <td>Vitae integer tempus condimentum.</td>
                                                                                        <td>19.99</td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td>Item Five</td>
                                                                                        <td>Ante turpis integer aliquet porttitor.</td>
                                                                                        <td>29.99</td>
                                                                                </tr>
                                                                        </tbody>
                                                                        <tfoot>
                                                                                <tr>
                                                                                        <td colspan="2"></td>
                                                                                        <td>100.00</td>
                                                                                </tr>
                                                                        </tfoot>
                                                                </table>
                                                        </div>
                                                </section>

                                                <section>
                                                        <h4>Buttons</h4>
                                                        <ul class="actions">
                                                                <li><a href="#" class="button special">Special</a></li>
                                                                <li><a href="#" class="button">Default</a></li>
                                                        </ul>
                                                        <ul class="actions">
                                                                <li><a href="#" class="button big">Big</a></li>
                                                                <li><a href="#" class="button">Default</a></li>
                                                                <li><a href="#" class="button small">Small</a></li>
                                                        </ul>
                                                        <ul class="actions fit">
                                                                <li><a href="#" class="button special fit">Fit</a></li>
                                                                <li><a href="#" class="button fit">Fit</a></li>
                                                        </ul>
                                                        <ul class="actions fit small">
                                                                <li><a href="#" class="button special fit small">Fit + Small</a></li>
                                                                <li><a href="#" class="button fit small">Fit + Small</a></li>
                                                        </ul>
                                                        <ul class="actions">
                                                                <li><a href="#" class="button special icon fa-download">Icon</a></li>
                                                                <li><a href="#" class="button icon fa-download">Icon</a></li>
                                                        </ul>
                                                        <ul class="actions">
                                                                <li><span class="button special disabled">Special</span></li>
                                                                <li><span class="button disabled">Default</span></li>
                                                        </ul>
                                                </section>

                                                <section>
                                                        <h4>Form</h4>
                                                        <form method="post" action="#">
                                                                <div class="row uniform 50%">
                                                                        <div class="6u 12u$(xsmall)">
                                                                                <input type="text" name="demo-name" id="demo-name" value="" placeholder="Name" />
                                                                        </div>
                                                                        <div class="6u$ 12u$(xsmall)">
                                                                                <input type="email" name="demo-email" id="demo-email" value="" placeholder="Email" />
                                                                        </div>
                                                                        <div class="12u$">
                                                                                <div class="select-wrapper">
                                                                                        <select name="demo-category" id="demo-category">
                                                                                                <option value="">- Category -</option>
                                                                                                <option value="1">Manufacturing</option>
                                                                                                <option value="1">Shipping</option>
                                                                                                <option value="1">Administration</option>
                                                                                                <option value="1">Human Resources</option>
                                                                                        </select>
                                                                                </div>
                                                                        </div>
                                                                        <div class="4u 12u$(small)">
                                                                                <input type="radio" id="demo-priority-low" name="demo-priority" checked>
                                                                                <label for="demo-priority-low">Low Priority</label>
                                                                        </div>
                                                                        <div class="4u 12u$(small)">
                                                                                <input type="radio" id="demo-priority-normal" name="demo-priority">
                                                                                <label for="demo-priority-normal">Normal Priority</label>
                                                                        </div>
                                                                        <div class="4u$ 12u(small)">
                                                                                <input type="radio" id="demo-priority-high" name="demo-priority">
                                                                                <label for="demo-priority-high">High Priority</label>
                                                                        </div>
                                                                        <div class="6u 12u$(small)">
                                                                                <input type="checkbox" id="demo-copy" name="demo-copy">
                                                                                <label for="demo-copy">Email me a copy of this message</label>
                                                                        </div>
                                                                        <div class="6u$ 12u$(small)">
                                                                                <input type="checkbox" id="demo-human" name="demo-human" checked>
                                                                                <label for="demo-human">I am a human and not a robot</label>
                                                                        </div>
                                                                        <div class="12u$">
                                                                                <textarea name="demo-message" id="demo-message" placeholder="Enter your message" rows="6"></textarea>
                                                                        </div>
                                                                        <div class="12u$">
                                                                                <ul class="actions">
                                                                                        <li><input type="submit" value="Send Message" class="special" /></li>
                                                                                        <li><input type="reset" value="Reset" /></li>
                                                                                </ul>
                                                                        </div>
                                                                </div>
                                                        </form>
                                                </section>

                                        </section>
-->				