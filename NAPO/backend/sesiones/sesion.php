<?php
    session_start();
    //Evaluo que la sesion continue, verificando una de las variables creadas en control.php, 
    //cuando esta ya no coincida con su valor inicial se redirije al archivo de salir.
    
    if(!$_SESSION["autentificado"]){//si su valor es false
       header("Location: /tandasnapo/NAPO/backend/sesiones/salir.php");
    }
?>