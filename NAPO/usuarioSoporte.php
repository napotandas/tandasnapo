<!DOCTYPE HTML>
<?php 
error_reporting(E_ALL ^ E_NOTICE);
include ("./backend/sesiones/sesion.php"); 
?>
<html>
	<head>
		<title>NAPO | SOPORTE</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="css/main2.css" />
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body id="top">

		<!-- Header -->
			<header id="header">
				<a href="usuarioInicio.php" class="image avatar"><img src="images/avatar.jpg" alt="" /></a>
                                <h1><strong>Usuario: </strong><?php echo ($_SESSION["aliasUsuario"]);?><br />
				Nivel: <?php echo ($_SESSION["nivelUsuario"]);?><br /><br/>
				Etapa: <?php echo ($_SESSION["etapaUsuario"]);?><br />
                                </h1>
			</header>
               <!-- Main User -->
               <div id="main-user">    
                   <ul class="nav">
                       <li><a href="#"><i class="fa fa-user"></i> Mi Cuenta</a>
                            <ul>
                                <li><a href="usuarioConfig.php">Ajustes</a></li>
                                <li><a href="index.php">Salir</a></li>
                            </ul>                      
                        </li>
                       
                        <li><a href="#"><i class="fa fa-bell"></i> Notiificaciones</a>
                            <ul>
                                <li><a href="usuarioNotificaciones.php">...</a></li>
                            </ul>                      
                        </li>        
                       
                        <li><a href="">Ayuda</a>
                            <ul>
                                <li><a href="usuarioSoporte.php">Soporte</a></li>
                                <li><a href="usuarioPF.php">Preguntas Frecuentes</a></li>
                            </ul>
                        </li>   
                   </ul>
                </div>
               
                <!-- Main -->
                <div id="main">
                    <section id="three">
                        <h2>
                            Soporte
                        </h2>
                        ¡Escr&iacute;benos!, te vamos a leer y trataremos de ayudarte...
                        <form action="backend/" name="registro-usuarios_frm" method="post" enctype="application/x-www-form-urlencoded" align="">
                            <div class="row" >
                                <div class="6u 12u(mobile)">
                                    <br>Asunto:
                                        <input name="Asunto_txt" placeholder="Asunto" type="text" maxlength="25" autocomplete="off" pattern="^([0-9a-zA-Z]){0,25}$" title="¿Cu&aacute;l es tu duda?"required/>
                                    <br>Mensaje:
                                        <textarea rows="10" name="Mensaje_txt" placeholder="Escribir mensaje..." required></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row 50%" align="right">
                                <div class="6u 12u" style="text-align:left;">
                                </div>
                                <div class="6u 12u">
                                    <ul class="actions">
                                        <li><input type="submit" name="enviar_btn" value="Enviar" /></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </section>
				
                    <section id="three">
                        <h2>Tus Datos</h2>
                            <div class="">
                                <ul class="labeled-icons">
                                    <li>
                                            <h3 class="icon fa-male"><span class="label">Usuario:</span></h3>
                                            <?php echo ($_SESSION["aliasUsuario"]);?>
                                    </li>
                                    <li>
                                        <h3 class="icon fa-mobile"><span class="label">Tu tel&eacute:fono</span></h3>
                                            <?php echo ($_SESSION["telefonoUsuario"]);?>
                                    </li>
                                    <li>
                                            <h3 class="icon fa-envelope-o"><span class="label">Tu Email:</span></h3>
                                            <a href="#"><?php echo ($_SESSION["emailUsuario"]);?></a>
                                    </li>
                                </ul>
                            </div>
                    </section>
                              

			</div>

		<!-- Footer -->
			<footer id="footer">
				
				<ul class="copyright">
					<li>&copy; NAPO S.A de C.V</li>
                                        <li><a href="mailito:soporte@napo.com.mx">soporte@napo.com.mx</a></li>
                                        
				</ul>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>