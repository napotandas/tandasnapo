<!DOCTYPE HTML>
<!--
isra
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
	huevos putos todos
-->
<html>
	<head>
        <title>TANDA + | ACCESO</title>
        <meta charset="utf-8" />
        <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/main.css" />
	</head>
	<body class="homepage">
		<div id="page-wrapper">

            <!-- Header -->            
            <div id="header-wrapper">
                <div id="header" class="container">

                    <!-- Logo -->
                    <h1 id="logo"><a href="index.php">tanda <i class="icon fa-plus" style="font-size:70%;"></i></a></h1>

                    <!-- Nav -->
                    <nav id="nav">
                        <ul>
                            <li><a href="ayuda.php#seccionAyuda">Ayuda</a></li>
                            <li>
                                <a href="nosotros.php#seccionNosotros">Nosotros</a>
                            </li>
                            <li class="break"><a href="ingresar.php#seccionAcceso">Ingresar</a></li>
                            <li><a href="registro.php#seccionRegistro">Registro</a></li>
                        </ul>
                    </nav>

                </div>

                <!-- Hero -->
                <section id="hero" class="container">
                    <header>
                        <img src="images/logo.png">
                        <h2>Bienvenido a <strong>tanda <i class="icon fa-plus" style="font-size:70%;"></i> </strong>
                            <br />
                            sitio de tandas online lider en M&eacute;xico.</h2>
                    </header>
                    <p>Si buscar mejorar tus <strong>ingresos</strong> y tu <strong>calidad de vida</strong>
                        <br />
                        <strong>has llegado al lugar indicado </strong></p>
                    <ul class="actions">
                        <li><a href="ayuda.php#seccionAyuda" class="button">¡M&uacute;estrame c&oacute;mo!</a></li>
                    </ul>
                </section>

            </div>

<!-- Footer -->
<a name="seccionAcceso"></a> 
    <div   id="footer-wrapper" style="padding-top:30px;">
        <div align="center" id="footer" class="container">
            <header class="major">
                    <h2>Accede a tu cuenta <strong>tanda <i class="icon fa-plus" style="font-size:50%;"></i></strong></h2>
                    <p>¡GRACIAS POR SER PARTE DE LA COMUNIDAD!.</p>
            </header>  
                        
                <div align="center" style="margin: -20px;">
                    <ul >
                        <img src="images/nosotros.png" style="width:20%;"> 
                    </ul>
                </div>
            
            <form  name="autentificacion_frm" method="post" action="backend/sesiones/control.php" enctype="application/x-www-form-urlencoded">
               <?php               
                error_reporting(E_ALL ^ E_NOTICE);
                if($_GET["Err"]=="ErrUsDB"){
                    echo "El usuario/contraseña no existen o están mal escritos";
                }else{
                    echo "Introduce tus datos";
                }                
                ?>
                
                
                <div class="6u 12u(mobile)" style="padding: 15px; ">
                    <input name="Usuario_txt" placeholder="Usuario" type="text" required/>
                </div>      
                <div class="6u 12u(mobile)" style="padding: 15px; ">
                    <input name="Contrasenia_txt" placeholder="Contraseña" type="password" required/>
                </div>
                <div class="6u 12u(mobile)" style="padding: 15px;" >
                    <ul class="actions" >
                        <li style="width:100%" ><input style="width:100%" type="submit" name="Accesar_btn" value="Accesar" /> </li>
                    </ul>
                    <a href="contraseniaOlvidada.php">¿Olvidaste tu contraseña?.</a>
                </div>
                <div class="6u 12u(mobile)" style="padding: 15px;" >
                    ¿No tienes cuenta en  <strong>tanda <i class="icon fa-plus" style="font-size:70%;"></i> </strong>?  <br><a href="registro.php#seccionRegistro">Registrarme.</a>
                </div>
            </form>
        </div>  
            <div id="copyright" class="container">
            <ul class="menu">
               <li>&copy; <strong>tanda <i class="icon fa-plus" style="font-size:50%;"></i></strong> S.A. de R.L.  Todos los derechos reservados.</li>
            </ul>
        </div>
    </div>

		</div>

		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>