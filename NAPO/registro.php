<!DOCTYPE HTML>
<!--
        Telephasic by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>TANDA + | REGISTRO</title>
        <meta charset="utf-8" />
        <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/main.css" />
    </head>
    <body class="homepage">
        <div id="page-wrapper">

            <!-- Header -->            
            <div id="header-wrapper">
                <div id="header" class="container">

                    <!-- Logo -->
                    <h1 id="logo"><a href="index.php">tanda <i class="icon fa-plus" style="font-size:70%;"></i></a></h1>

                    <!-- Nav -->
                    <nav id="nav">
                        <ul>
                            <li><a href="ayuda.php#seccionAyuda">Ayuda</a></li>
                            <li>
                                <a href="nosotros.php#seccionNosotros">Nosotros</a>
                            </li>
                            <li class="break"><a href="ingresar.php#seccionAcceso">Ingresar</a></li>
                            <li><a href="registro.php#seccionRegistro">Registro</a></li>
                        </ul>
                    </nav>

                </div>

                <!-- Hero -->
                <section id="hero" class="container">
                    <header>
                        <img src="images/logo.png">
                        <h2>Bienvenido a <strong>tanda <i class="icon fa-plus" style="font-size:70%;"></i> </strong>
                            <br />
                            sitio de tandas online lider en M&eacute;xico.</h2>
                    </header>
                    <p>Si buscar mejorar tus <strong>ingresos</strong> y tu <strong>calidad de vida</strong>
                        <br />
                        <strong>has llegado al lugar indicado </strong></p>
                    <ul class="actions">
                        <li><a href="ayuda.php#seccionAyuda" class="button">¡M&uacute;estrame c&oacute;mo!</a></li>
                    </ul>
                </section>

            </div>

            <!-- Footer -->
            <a name="seccionRegistro"></a> 
            <div id="footer-wrapper" style="padding-top:50px;">
                <div id="footer" class="container" >
                    <header class="major">
                        <h2>Registro de Usuarios</h2>
                        <p>¡UNETE!<br />
                            ¡Es fácil y rápido! Comienza a recibir regalos.</p>
                    </header>
                    <div class="row">
                        <section class="6u 12u(mobile)" >
                            <div align="center">
                                <ul align="center">
                                    Mismo resumen del video 
                                </ul>
                            </div>
                        </section>

                        <section class="6u 12u(narrower)">
                            <form action="backend/usuarios/registroUsuarios.php" name="registro-usuarios_frm" method="post" enctype="application/x-www-form-urlencoded">
                                <div class="row 50%">
                                    <div class="6u 12u(mobile)">
                                        <input name="Alias_txt" placeholder="*Alias" type="text" maxlength="10" autocomplete="off" pattern="^([0-9a-zA-Z]){4,10}$" title="El alias debe contener almenos 4 numeros y/o letras."required/>
                                    </div>
                                </div>
                                <div class="row 50%">
                                    <div class="6u 12u(mobile)">
                                        <input name="Contrasenia_txt" placeholder="*Contrasenia" type="password" id="password1" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" autocomplete="off" title="La contraseña debe contener almenos una MAYUSCULA, una minuscula y un numero. Debe ser mayor a 8 caracteres." required/>
                                    </div>
                                    <div class="6u 12u(mobile)">
                                        <input name="RepetirContraseña_txt" placeholder="*Repetir Contraseña" type="password" id="password2" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" autocomplete="off" title="La contraseña debe contener almenos una MAYUSCULA, una minuscula y un numero. Debe ser mayor a 8 caracteres." required/>
                                    </div>
                                </div>
                                <div class="row 50%">
                                    <div class="6u 12u(mobile)">
                                        <input name="Email_txt" placeholder="*Email" type="email" autocomplete="off" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required/>
                                    </div>
                                    <div class="6u 12u(mobile)">
                                        <input name="Teléfono_txt" placeholder="*Telefono" type="tel"  lang="es" autocomplete="off" maxlength="10" pattern="[0-9]{10}" title="El telefono debe contener 10 digitos." required/>
                                    </div> 
                                </div>
                                <div class="row 50%" align="right">
                                    
                                    <div class="6u 12u" style="text-align:left;">
                                        <a href="ingresar.php#seccionAcceso">Ya tengo una cuenta en  <strong>tanda <i class="icon fa-plus" style="font-size:70%;"></i> </strong>.</a>
                                    </div>
                                    <div class="6u 12u">
                                        <ul class="actions">
                                            <li><input type="submit" name="registrar_btn" value="Unirme" /></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </section>
                    </div>
                </div>  
                <div id="copyright" class="container">
                    <ul class="menu">
                       <li>&copy; <strong>tanda <i class="icon fa-plus" style="font-size:50%;"></i></strong> S.A. de R.L.  Todos los derechos reservados.</li>
                    </ul>
                </div>
            </div>

        </div>

        <!-- Scripts -->

        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
        <script src="assets/js/main.js"></script>
        <script>
            var password1 = document.getElementById('password1');
            var password2 = document.getElementById('password2');

            var checkPasswordValidity = function() {
                if (password1.value != password2.value) {
                    password1.setCustomValidity('Las contraseñas deben coincidir.');
                } else {
                    password1.setCustomValidity('');
                }        
            };

            password1.addEventListener('change', checkPasswordValidity, false);
            password2.addEventListener('change', checkPasswordValidity, false);

            var form = document.getElementById('passwordForm');
            form.addEventListener('submit', function() {
                checkPasswordValidity();
                if (!this.checkValidity()) {
                    event.preventDefault();
                    //Implement you own means of displaying error messages to the user here.
                    alert("Las contraseñas no coinciden");
                    password1.focus();
                }
            }, false);
        </script>
    </body>
</html>