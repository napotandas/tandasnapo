<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
            <title>TANDA + | AYUDA</title>
            <meta charset="utf-8" />
            <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="stylesheet" href="css/main.css" />
	</head>
	<body class="homepage">
		<div id="page-wrapper">

            <!-- Header -->            
           <div id="header-wrapper">
                <div id="header" class="container">

                    <!-- Logo -->
                    <h1 id="logo"><a href="index.php">tanda <i class="icon fa-plus" style="font-size:70%;"></i></a></h1>

                    <!-- Nav -->
                    <nav id="nav">
                        <ul>
                            <li><a href="ayuda.php#seccionAyuda">Ayuda</a></li>
                            <li>
                                <a href="nosotros.php#seccionNosotros">Nosotros</a>
                            </li>
                            <li class="break"><a href="ingresar.php#seccionAcceso">Ingresar</a></li>
                            <li><a href="registro.php#seccionRegistro">Registro</a></li>
                        </ul>
                    </nav>

                </div>

                <!-- Hero -->
                <section id="hero" class="container">
                    <header>
                        <img src="images/logo.png">
                        <h2>Bienvenido a <strong>tanda <i class="icon fa-plus" style="font-size:70%;"></i> </strong>
                            <br />
                            sitio de tandas online lider en M&eacute;xico.</h2>
                    </header>
                    <p>Si buscar mejorar tus <strong>ingresos</strong> y tu <strong>calidad de vida</strong>
                        <br />
                        <strong>has llegado al lugar indicado </strong></p>
                    <ul class="actions">
                        <li><a href="ayuda.php#seccionAyuda" class="button">¡M&uacute;estrame c&oacute;mo!</a></li>
                    </ul>
                </section>

            </div>

    <!-- Footer -->
    <a name="seccionAyuda"></a> 
        <div id="footer-wrapper" style="padding-top:50px;">
            <div id="footer" class="container" >
                <header class="major">
                    <h2>200 pesitos + 2 referidos = 3200 pesotes</h2>
                    <p style="color: #27636B">¿Tienes dudas?<br />
                    ¡Mira este video hasta el final!</p>
                </header>
                <div class="row">
                    <section class="6u 12u(mobile)" >
                        <div align="center">
                            <ul align="center">
                                
                                <img src="images style="width:19%;">
                                     
                                 <img src="images/.png">
                            </ul>
                        </div>
                    </section>

                    <section class="6u 12u(narrower)">
                        <iframe width="454" height="300" src="https://www.youtube.com/embed/lVTTXmyJCMw" frameborder="0" allowfullscreen></iframe>
                    </section>
                </div>
            </div>  

<!--            <div id="footer" class="container" >
                <header class="major" style="padding-top:10%;">
                    <h2>Contacto</h2>
                    <h3 style="color: #27636B">¡Escr&iacute;benos!</h3>
                    <br>¿A&uacute;n tienes dudas? <br>Mandanos un correo y en breve nos pondremos en contacto contigo.
                </header>
                    <div class="row">
                        <section class="6u 12u(narrower)">
                            <form action="backend/" name="registro-usuarios_frm" method="post" enctype="application/x-www-form-urlencoded">
                                <div class="row 50%">
                                    <div class="6u 12u(mobile)">
                                        <br>Nombre:
                                        <input name="Nombre_txt" placeholder="Nombre" type="text" maxlength="25" autocomplete="off" pattern="^([a-zA-Z]){4,10}$" title="Escr&iacute;be tu nombre."required/>
                                        <br>Correo electronico:
                                        <input name="Email_txt" placeholder="Correo electronico" type="email" autocomplete="off" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required/>
                                        <br>Asunto:
                                        <input name="Asunto_txt" placeholder="Asunto" type="text" maxlength="25" autocomplete="off" pattern="^([0-9a-zA-Z]){0,25}$" title="¿Cu&aacute;l es tu duda?"required/>
                                         
                                    </div>
                                    <div class="6u 12u(mobile)">
                                        <br>Mensaje:
                                        <textarea rows="10" name="Mensaje_txt" placeholder="Escribir mensaje..." required></textarea>
                                    </div>
                                </div>
                                <div class="row 50%" align="right">
                                    
                                    <div class="6u 12u" style="text-align:left;">
                                        
                                    </div>
                                    <div class="6u 12u">
                                        <ul class="actions">
                                            <li><input type="submit" name="enviar_btn" value="Enviar" /></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </section>
                        
                        <section class="6u 12u(mobile)" >
                            <div align="center">
                                <ul align="center">
                                    <img src="images/juanEscritorio2.png" style="width:40%;"> 
                                </ul>
                            </div>
                        </section>
                    </div>
             </div>-->
            
            <div id="copyright" class="container">
                <ul class="menu">
                    <li>&copy; <strong>tanda <i class="icon fa-plus" style="font-size:50%;"></i></strong> S.A. de R.L.  Todos los derechos reservados.</li>
                </ul>
            </div>
				</div>

		</div>

		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>